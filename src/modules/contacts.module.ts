import { Module } from "@nestjs/common";
import { ContactsController } from '@ppi-controllers/contacts.controller';
import { ContactsService } from '@ppi-services/contacts.service';

@Module({
    imports: [],
    controllers: [ContactsController],
    providers: [ContactsService]
})
export class ContactsModule {}