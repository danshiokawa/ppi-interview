import { Injectable } from '@nestjs/common';
import * as contactsFile from '../../contacts.json';

@Injectable()
export class ContactsService {

    /**
     * Function to retrieve a specific office based on a query string
     * @param queryString Query string provided by the user
     * @returns JSON formatted object with office information
     */
    findOffice(queryString: string): any {
        for (let i = 0; i < contactsFile.length; i++) {
            const contact = contactsFile[i];
            if (contact && contact.office.includes(queryString)) {
                return contact;
            }
        }

        return {};
    }

    /**
     * Function to retrieve a specific contact based on a query string
     * @param queryString Query string provided by the user
     * @returns JSON formatted object with contact information
     */
    findContact(queryString: string): any {
        for (let i = 0; i < contactsFile.length; i++) {
            if (contactsFile[i] && contactsFile[i].contacts_by_category) {
                for (let j = 0; j < contactsFile[i].contacts_by_category.length; j++) {
                    const contact = this.findContactInCategory(contactsFile[i].contacts_by_category[j], queryString);
                    if (contact) return contact;
                }
            }
        }

        return {};
    }

    /**
     * [PRIVATE] Function to find contact inside a category
     * @param category 
     * @param searchTerm 
     */
    private findContactInCategory(category: any, searchTerm: string): any {
        if (category && category.contacts) {
            for (let i = 0; i < category.contacts.length; i++) {
                if(category.contacts[i]) {
                    if (category.contacts[i].name.includes(searchTerm)) {
                        return category.contacts[i];
                    }
                }
            }
            return;
        }
    }
}