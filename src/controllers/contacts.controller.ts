import { Controller, Get, Req } from '@nestjs/common'
import { ContactsService } from '@ppi-services/contacts.service';


@Controller()
export class ContactsController {
    constructor(private readonly service: ContactsService) {}

    /**
     * [GET] Endpoint to find the specific office
     * @param request HTTP request provided
     * @returns JSON formatted object
     */
    @Get('api/contacts/office')
    findOffice(@Req() request: any): any {
        return this.service.findOffice(decodeURI(request._parsedUrl.query));
    }

    /**
     * [GET] Endpoint to find the specific contact
     * @param request HTTP request provided
     * @returns JSON formatted object
     */
    @Get('api/contacts/contact')
    findContact(@Req() request: any): any {
        return this.service.findContact(decodeURI(request._parsedUrl.query));
    }
}