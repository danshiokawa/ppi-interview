import { Test, TestingModule } from '@nestjs/testing';

// Not using alias to test path routes
import { ContactsController } from './contacts.controller';
import { ContactsService } from '../services/contacts.service';

const mockDummyRequest = {
    _parsedUrl: {
        query: 'test123'
    }
}

const mockValidRequest = {
    _parsedUrl: {
        query: 'Sean'
    }
}

describe('ContactsController', () => {
  let contactsController: ContactsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ContactsController],
      providers: [ContactsService],
    }).compile();

    contactsController = app.get<ContactsController>(ContactsController);
  });

  describe('root', () => {
    it('should return nothing with and invalid request', () => {
      expect(contactsController.findContact(mockDummyRequest)).toStrictEqual({});
    });

    it('should return data with and a valid request', () => {
        expect(contactsController.findContact(mockValidRequest)).toBeTruthy();
    });
  });
});
