# PPI Interview (RestAPI)

PPI interview project - RESTful API

### Prerequisites

This repo is public, please clone this repo and execute **npm i** to install all the packages. Developed in NESTjs / NodeJS / Typescript

## Execution

1- After installed all packages, run the server with **npm run start**

2- Go to your browser and access the route:

**http://localhost/3000/api/contacts/contact?Sean%20Carey** OR **http://localhost/3000/api/contacts/contact?Sean%20C** to retrieve the contact information

(PS: the **search** functionality is implemented, you can look for "Rob Burns", for instance. **http://localhost:3000/api/contacts/contact?Burns** )

AND

**http://localhost/3000/api/contacts/office?Vancouver** to retrieve the office information


### Interview original description

The PPI public website [contacts page](https://www.ppi.ca/en/contact) contains a list of all our offices with addresses, key contacts and geo location info.

It doesn’t yet have a much requested ‘search’ feature.

The contact info is stored in an Azure table on the production web site, and is provided in the json file included in this project.

For the purposes of this test please create a REST API that provides a simple search capability and returns either an office or a contact (person)

For example, if the request is [https://example.com/api?query=Sean%20Carey](https://example.com/api?query=Sean%20Carey) , the api should return the contact object for him:

     {
        “name”: “Sean Carey”,
        “title”: “Vice-President, Sales - British Columbia”,
        “phone”: “778-374-3501”,
        “email”: “ [scarey@ppi.ca](mailto:scarey@ppi.ca) “
     }

For an office query, [https://example.com/api?query=Vancouver](https://example.com/api?query=Vancouver) , return the entire json object for ‘Vancouver’.

Build the **simplest** possible solution or MVP (minimal viable product). It doesn't need any error handling or other clever features and can simply load the provided json file on startup and assume it will never change.

Use any open source technology and libraries you deem appropriate e.g. Node/Express, .Net Core MVC, Python/Flask

Do not spend more than a couple of hours on this task, it should be an MVP and nothing more.

Clone the provided repo, and send a link to a publicly available version of it when you have completed the project. Provide basic instructions on how to run the completed project on a local
development machine.

