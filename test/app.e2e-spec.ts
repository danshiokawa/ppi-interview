import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { ContactsModule } from './../src/modules/contacts.module';

describe('ContactsController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [ContactsModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/api/contacts/office (GET)', () => {
    return request(app.getHttpServer())
      .get('/api/contacts/office')
      .expect(200);
  });

  it('/api/contacts/contact (GET)', () => {
    return request(app.getHttpServer())
      .get('/api/contacts/contact')
      .expect(200);
  });
});
